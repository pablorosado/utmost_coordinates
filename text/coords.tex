%\documentclass[10pt,aps,prd,twocolumn,superscriptaddress,showpacs]{revtex4-1}
\documentclass[12pt,a4paper]{article}

%%%%%%%%%%%%%%%%%%%%%
%Define packages and bib style:
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{xspace}
\usepackage[usenames]{color}
\usepackage{url}

\bibliographystyle{plain}

%%%%%%%%%%%%%%%%%%%%%
%Define some paths and commands:
\def \plots {./plots/}

\newcommand{\rosa}[1]{\textcolor{blue}   {$\to$ #1} }

%%%%%%%%%%%%%%%%%%%%%
%Define title, date and authors:
\begin{document}
\title{\textbf{Coordinate transformations for UTMOST}}
\author{Pablo A. Rosado}
\date{\today}
\maketitle

\section{\label{sc:definitions} Definitions}

In this document I will derive direct and inverse coordinate transformations between sky coordinates $(H, \delta)$ and telescope coordinates $(T,M)$ of a point $Q$, using the following definitions and criteria:
\begin{itemize}
\item All angles are given in radians.
\item East and West refer to Earth (not celestial) ones.
\item All coordinates are given with respect to right-handed Cartesian systems.
\item A rotation of a positive angle will be counter-clockwise about the axis of rotation.
\item $\mathcal{M}$ is the meridian passing through the centre of the telescope.
\item $\mathcal{P}$ is the parallel passing through the centre of the telescope.
\item $\Pi$ is the telescope plane, that contains the North-South and East-West arms.
\item $\Sigma$ is the plane tangent to both $\mathcal{M}$ and $\mathcal{P}$ at the telescope site. 
\item $H$ is the hour angle of $Q$.
\item $\delta$ is the declination of $Q$.
\item $T$ is the telescope North-South tilt angle of $Q$, which grows towards the North. 
\item $M$ is the telescope meridian distance, which grows towards the West.
\item $\lambda$ is the telescope latitude.
\item $\eta$ is the telescope \textit{skew} (aka. azimuth), which is the angle between $\mathcal{P}$ and the projection of the East-West arm onto $\Sigma$.
In other words, $\eta$ is the angle between $\mathcal{M}$ and the telescope North-South arm.
\item $\xi$ is the telescope \textit{slope}, which is the angle between $\Pi$ and $\Sigma$.
\item The rotation matrices are defined for passive (alias) rotations:
\begin{align}
\label{eq:rotations}
R_{1}(\alpha)=\left[ \begin{array}{ccc}
1 & 0 & 0 \\
0 & \cos \alpha & \sin \alpha \\
0 & -\sin \alpha & \cos \alpha \end{array} \right], \nonumber \\
R_2(\alpha)=\left[ \begin{array}{ccc}
\cos \alpha & 0 & -\sin \alpha \\
0 & 1 & 0 \\
\sin \alpha & 0 & \cos \alpha \end{array} \right], \\
R_3(\alpha)=\left[ \begin{array}{ccc}
\cos \alpha & \sin \alpha & 0 \\
-\sin \alpha & \cos \alpha & 0 \\
0 & 0 & 1 \end{array} \right]. \nonumber
\end{align}
The use of these matrices can be clarified with an example:
Consider two coordinate systems $S$ and $S'$, whose origins are in the same point.
System $S$ has axes $X_1$, $X_2$, and $X_3$.
System $S'$, with axes $X_1'$, $X_2'$, and $X_3'$, is the result of rotating $S$ around $X_1$ by an angle $\alpha$ (so that axes $X_1$ and $X_1'$ are identical).
Then, a point $Q$ described by coordinates $\mathbf{x}$ in system $S$ is described in system $S'$ by coordinates $\mathbf{x'}=R_1(\alpha)\mathbf{x}$.
\item Similarly, the inverse rotation matrices are defined by
\begin{align}
\label{eq:invrotations}
R_{1}^{-1}(\alpha)=R_1(-\alpha), \nonumber \\
R_2^{-1}(\alpha)=R_2(-\alpha), \\
R_3^{-1}(\alpha)=R_3(-\alpha). \nonumber
\end{align}
\end{itemize}

\section{\label{sc:transformations} Direct and inverse transformations}

Let us consider a point $Q$ in the sky, described in a coordinate system $S$ by coordinates $\mathbf{x}=[x_1, x_2, x_3]$.
This coordinate system $S$, with axes $X_1$, $X_2$, and $X_3$, is located on the surface of Earth at its equator, and:
\begin{itemize}
\item $X_1$ is in the plane of the equator and grows towards the West.
\item $X_2$ is perpendicular to the surface of Earth and grows outwards from the centre of Earth.
\item $X_3$ is perpendicular to the plane of the equator and grows towards the North.
\end{itemize}
By definition, the sky coordinates $(H,\delta)$ of $Q$ are related to $\mathbf{x}$ by
\begin{equation}
\label{eq:xdef}
\mathbf{x}=\left[ \begin{array}{c}
x_1 \\
x_2 \\
x_3 \end{array}\right]=\left[ \begin{array}{c}
\cos\delta \sin H \\
\cos \delta \cos H \\
\sin \delta \end{array}\right].
\end{equation}

The telescope is not at the equator, so, in order to define a new coordinate system at the position of the telescope, we would need to perform a translation and a rotation.
Given that $Q$ is distant, we neglect the effect of the translation (i.e. we neglect the radius of Earth with respect to the distance to any astronomical object).
We hence define a new system $S'$, with axes $X_1'$, $X_2'$, and $X_3'$, so that
\begin{itemize}
\item $X_1'$ is parallel to the equator (and hence parallel to $X_1$).
\item $X_2'$ is perpendicular to $\Sigma$ and grows outwards from the centre of Earth.
\item $X_3'$ is tangent to $\mathcal{M}$ and grows towards the North.
\end{itemize}
System $S'$ is hence obtained by rotating $S$ about $X_1$ by an angle $\lambda$, and the coordinates of $Q$ in this system are
\begin{equation}
\label{eq:x2xp}
\mathbf{x'}=R_1(\lambda)\mathbf{x}.
\end{equation}
The origin of $S'$ is assumed to be at the centre of the telescope (i.e. we assume that the origin of $S$ was also intersected by $\mathcal{M}$, which is a consequence of the choice of $H=0$ for all objects intersecting the plane of $\mathcal{M}$).

\noindent\fbox{
    \parbox{\textwidth}{
 $\to$ There is no doubt about the sign of $\lambda$: given that the telescope is in the southern hemisphere, and given our (standard) definitions of $H$ and $\delta$, then $\lambda<0$.
 }}

The East-West arm of the telescope is actually not parallel to the equator; instead, the easternmost side is tilted by an angle $\eta$.
We define a system $S''$ that is obtained by rotating $S'$ about $X_2'$ by an angle $\eta$, so that
\begin{itemize}
\item $X_1''$ is parallel to a projection of the East-West arm onto $\Sigma$ (and hence $X_1''$ is misaligned with $\mathcal{P}$).
\item $X_2''$ is equivalent to $X_2'$.
\item $X_3''$ is now aligned with the North-South arm of the telescope (and hence misaligned with $\mathcal{M}$).
\end{itemize}
Then, the coordinates of $Q$ in $S''$ are 
\begin{equation}
\label{eq:xp2xpp}
\mathbf{x''}=R_2(\eta)\mathbf{x'}.
\end{equation}

\noindent\fbox{
\parbox{\textwidth}{
The sign of $\eta$ seems to be unclear (we will discuss about it in the following section), but we can conclude that:

$\to$ If $\eta>0$, the easternmost side is tilted towards the North.

$\to$ If $\eta<0$, the easternmost side is tilted towards the South.
}}

One of the extremes of the East-West arm is higher than the other (i.e. the East-West arm is not parallel to $\Sigma$).
We define a system $S'''$ that is obtained by rotating $S''$ about $X_3''$ by an angle $\xi$, so that
\begin{itemize}
\item $X_1'''$ is now aligned with the telescope East-West arm and grows positive towards the West.
\item $X_2'''$ is perpendicular to $\Pi$ and grows towards the sky.
\item $X_3'''$ is aligned with the telescope North-South arm and grows towards the North.
\end{itemize}
Then, the coordinates of $Q$ in $S'''$ are
\begin{equation}
\label{eq:xpp2xppp}
\mathbf{x'''}=R_3(\xi)\mathbf{x''}.
\end{equation}

\noindent\fbox{
\parbox{\textwidth}{
The sign of $\xi$ also seems to be unclear (we will discuss about it in the following section), but we can conclude that:

$\to$ If $\xi>0$, the easternmost side is lower than the ground level.

$\to$ If $\xi<0$, the easternmost side is higher than the ground level.
}}

By definition, the telescope coordinates $(T,M)$ of $Q$ are related to $\mathbf{x'''}$ by
\begin{equation}
\label{eq:xpppdef}
\mathbf{x'''}=\left[ \begin{array}{c}
x_1''' \\
x_2''' \\
x_3''' \end{array}\right]=\left[ \begin{array}{c}
\sin M \\
\cos M \cos T \\
\cos M \sin T \end{array}\right] .
\end{equation}

Putting together Eqs. (\ref{eq:x2xp}), (\ref{eq:xp2xpp}) and (\ref{eq:xpp2xppp}), we obtain
\begin{equation}
\label{eq:xpppmats}
%\mathbf{x'''}=R_3(\xi)\mathbf{x''}=R_3(\xi) R_2(\eta)\mathbf{x'}=R_3(\xi) R_2(\eta)R_1(\lambda)\mathbf{x}.
\mathbf{x'''}=R_3(\xi) R_2(\eta)R_1(\lambda)\mathbf{x}.
\end{equation}
This equation also implies
\begin{equation}
\label{eq:xpmats}
\mathbf{x}=R_1^{-1}(\lambda)R_2^{-1}(\eta)R_3^{-1}(\xi)\mathbf{x'''}.
\end{equation}
These equations can be explicitly written by components as
\footnotesize
\begin{equation}
\label{eq:xppptrans}
\hspace*{-2cm}\left[ \begin{array}{c}
x_1''' \\
x_2''' \\
x_3''' \end{array}\right]=\left[ \begin{array}{c}
\sin H \cos \delta \cos \eta \cos \xi+\cos H \cos \delta (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi) +\sin \delta (\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi) \\
-\sin H \cos \delta \cos \eta \sin \xi + \cos H \cos \delta (\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi) + \sin \delta (\sin \lambda \cos \xi +\cos \lambda \sin \eta \sin \xi) \\
\sin H \cos \delta \sin \eta-\cos H \cos \delta \sin \lambda \cos \eta + \sin \delta \cos \lambda \cos \eta \end{array}\right] ,
\end{equation}
\normalsize
and
\footnotesize
\begin{equation}
\label{eq:xtrans}
\hspace*{-2cm}\left[ \begin{array}{c}
x_1 \\
x_2 \\
x_3 \end{array}\right]=\left[ \begin{array}{c}
\sin M \cos \eta \cos \xi + \sin T \cos M \sin \eta -\cos T \cos M \cos \eta \sin \xi \\
-\sin T \cos M \sin \lambda \cos \eta+\cos T \cos M(\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi)+\sin M (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi) \\
\sin T \cos M \cos \lambda \cos \eta +\cos T \cos M (\sin \lambda \cos \xi+\cos \lambda \sin \eta \sin \xi)+\sin M(\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi) \end{array}\right] .
\end{equation}
\normalsize

By inspecting Eq. (\ref{eq:xdef}), one can see that
\begin{align}
\label{eq:hadecdef}
H&=\arctan \left( \frac{x_1}{x_2} \right), \nonumber \\
\delta &=\arcsin \left( x_3 \right).
\end{align}
Similarly, by inspecting Eq. (\ref{eq:xpppdef}),
\begin{align}
\label{eq:tmdef}
T&=\arctan \left( \frac{x_3'''}{x_2'''}\right), \nonumber \\
M&=\arcsin \left( x_1'''\right).
\end{align}

Finally, combining Eqs. (\ref{eq:xppptrans}) and (\ref{eq:tmdef}), we obtain the telescope coordinates $(T,M)$ of $Q$ as a function of its sky coordinates $(H,\delta)$,
\[
\hspace*{-1cm}\boxed{
\begin{array}{l}
T=\arctan \left( \frac{\sin H \cos \delta \sin \eta-\cos H \cos \delta \sin \lambda \cos \eta + \sin \delta \cos \lambda \cos \eta}{-\sin H \cos \delta \cos \eta \sin \xi + \cos H \cos \delta (\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi) + \sin \delta (\sin \lambda \cos \xi +\cos \lambda \sin \eta \sin \xi)} \right), \vspace{0.5cm}\\

M=\arcsin \left( \sin H \cos \delta \cos \eta \cos \xi+\cos H \cos \delta (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi) +\right.\nonumber \\
 \left. \hspace*{2.3cm} \sin \delta (\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi)\right).
\end{array}}
\]
And similarly, combining Eqs. (\ref{eq:xtrans}) and (\ref{eq:hadecdef}), we obtain the sky coordinates $(H,\delta)$ of $Q$ as a function of its telescope coordinates $(T,M)$. 
\[
\hspace*{-1cm}\boxed{
\begin{array}{l}
H=\arctan \left( \frac{\sin M \cos \eta \cos \xi + \sin T \cos M \sin \eta -\cos T \cos M \cos \eta \sin \xi}{-\sin T \cos M \sin \lambda \cos \eta+\cos T \cos M(\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi)+\sin M (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi)} \right), \vspace{0.5cm}\\

\delta=\arcsin \left( \sin T \cos M \cos \lambda \cos \eta +\cos T \cos M (\sin \lambda \cos \xi+\cos \lambda \sin \eta \sin \xi)+\right. \nonumber \\
 \left. \hspace*{2cm} \sin M(\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi) \right).
\end{array}}
\]

The exact values of $\lambda$, $\xi$, and $\eta$ will be discussed in the following section.

\newpage

\section{Comparison with other documents and codes}

\subsection{Document `Coordinate transformations for MOST'}
Let us call it `the Doc'.
There one can find
\begin{align}
\sin M=& \cos \delta \sin H \cos \xi \cos \eta - \cos \delta \cos H (\sin \lambda \cos \xi \sin \eta + \cos \lambda \sin \xi) \nonumber \\
& - \sin \delta (\sin \lambda \sin \xi - \cos \lambda \cos \xi \sin \eta) \nonumber \\
=&\cos \delta \sin H (0.9999940546)- \cos \delta \cos H (2.798011806 \times 10^{-3}) \nonumber \\
& -\sin \delta (-2.015514993\times 10^{-3}),
\end{align}
\begin{align}
\cos M \sin T =&-\cos \delta \sin H \sin \eta  - \cos \delta \cos H \sin \lambda \cos \eta + \sin \delta \cos \lambda \cos \eta \nonumber\\
=& \cos \delta \sin H(-2.37558704 \times 10^{-5})  + \cos \delta \cos H (0.578881847) \nonumber\\
&+  \sin \delta (0.8154114339),
\end{align}
\begin{align}
\cos M \cos T = &  \cos \delta \sin H \cos \eta \sin \xi + \cos \delta \cos H (\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi) \nonumber\\
&+ \sin \delta (\cos \lambda \sin \eta \sin \xi + \sin \lambda \cos \xi)\nonumber\\
= & \cos \delta \sin H (3.448275861 \times 10^{-3}) + \cos \delta \cos H (0.8154066339) \nonumber\\
&- \sin \delta (0.578878472).
\end{align}

The numerical factors correspond to the following values of the parameters (where the uncertainties are ignored):
\begin{itemize}
\item $\lambda=-35.37193612^\circ = -0.6173567480991081$.
\item $\eta=4.9''=2.3755870374367263\times 10^{-5}$.
\item $\xi=11'51.26''=3.448285788259685\times 10^{-3}$.
\end{itemize}

\subsection{My code}
My formulas for $M$ and $T$ can be rewritten as
\begin{align}
\sin M=& \cos \delta \sin H \cos \xi \cos \eta -\cos \delta \cos H (-\sin \lambda \cos \xi \sin \eta-\cos \lambda \sin \xi)\nonumber \\
&- \sin \delta (-\sin \lambda \sin \xi+\cos \lambda \cos \xi \sin \eta ),
\end{align}
\begin{align}
&\cos M \sin T=\sin \eta \cos \delta \sin H  - \sin \lambda \cos \eta \cos \delta \cos H +  \cos \lambda \cos \eta \sin \delta,
\end{align}
\begin{align}
\cos M \cos T = &-\cos \delta \sin H \cos \eta \sin \xi + \cos \delta \cos H  (\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi) \nonumber \\
&+ \sin \delta (\cos \lambda \sin \eta \sin \xi+\sin \lambda \cos \xi).
\end{align}
If I define $\hat{\xi}=-\xi$ and $\hat{\eta}=-\eta$, I obtain
\begin{align}
\sin M=& \cos \delta \sin H \cos \hat{\xi} \cos \hat{\eta} -\cos \delta \cos H (\sin \lambda \cos \hat{\xi} \sin \hat{\eta}+\cos \lambda \sin \hat{\xi})\nonumber \\
&- \sin \delta (\sin \lambda \sin \hat{\xi}-\cos \lambda \cos \hat{\xi} \sin \hat{\eta} ),
\end{align}
\begin{align}
&\cos M \sin T=-\sin \hat{\eta} \cos \delta \sin H  - \sin \lambda \cos \hat{\eta} \cos \delta \cos H +  \cos \lambda \cos \hat{\eta} \sin \delta,
\end{align}
\begin{align}
\cos M \cos T = &\cos \delta \sin H \cos \hat{\eta} \sin \hat{\xi} + \cos \delta \cos H (\cos \lambda \cos \hat{\xi}-\sin \lambda \sin \hat{\eta} \sin \hat{\xi}) \nonumber \\
& +\sin \delta (\cos \lambda \sin \hat{\eta} \sin \hat{\xi}+\sin \lambda \cos \hat{\xi}).
\end{align}
These formulas are exactly equivalent to the ones in the Doc, as long as $\hat{\xi}$ and $\hat{\eta}$ are exactly the same as $\xi$ and $\eta$ of the Doc.

\subsection{John's code}
According to it,
\begin{align}
\label{eq:john1}
\sin M=& \cos \delta \sin H \cos \xi - \cos \delta \cos H (-\sin \lambda ~ \eta  + \cos \lambda \sin \xi)  \nonumber\\
&-\sin \delta (\sin \lambda \sin \xi +\cos \lambda ~ \eta ) )\nonumber\\
=&\cos \delta \sin H (0.9999940506)-\cos \delta \cos H (2.82652215778\times 10^{-3})  \nonumber\\
&-\sin \delta (-1.97739746177\times 10^{-3}),
\end{align}
\begin{align}
\label{eq:john2}
\cos M \sin T =&\cos \delta \sin H (-2.37558704\times 10^{-5}) +\cos \delta \cos H (0.578881847) + \nonumber\\
&\sin \delta (0.8154114339).
\end{align}
In his code, the values of the parameters are:
\begin{itemize}
\item $\lambda=-35.370707^\circ=-0.617335295908206$.
\item $\eta=4.9''$.
\item $\xi=1/289.9=3.4494653328734047\times 10^{-3}$.
\end{itemize}
One can see that $\eta$ is exactly the same as in the Doc, but $\lambda$ and $\xi$ are slightly different.

The second formula agrees with the one in the Doc exactly.
Even the numerical factors are exactly like in the Doc, which is concerning: this implies that the values of $\lambda$, $\eta$, and $\xi$ used in Eqs. (\ref{eq:john1}) and (\ref{eq:john2}) are (slightly) different.

Furthermore, the first formula does not agree with the one in the Doc for various reasons:
\begin{itemize}
\item Firstly, as already mentioned, the values of $\lambda$ and $\xi$ are slightly different (but the difference is probably within the uncertainty).
\item Some approximations are made: $\sin \eta \approx \eta$ and $\cos \eta \approx 1$.
\item There is a minus sign in the first formula that differs from the one in the Doc.
\end{itemize}

As a conclusion, either John's code is wrong (by a minus sign) or the Doc is.
Given that John's code is not totally consistent, I would tend to believe that the Doc is more accurate.

\subsection{Ewan's code}
According to `Full equation for HA, Dec to telescope NS,MD' in \url{https://github.com/ewanbarr/anansi/blob/master/docs/MolongloCoords.ipynb},
\begin{align}
\label{eq:ewan1}
%sin(EW) = (sin(lat)*sin(skew) - sin(slope)*cos(lat)*cos(skew))*cos(Dec)*cos(HA) + (-sin(lat)*sin(slope)*cos(skew) - sin(skew)*cos(lat))*sin(Dec) + sin(HA)*cos(Dec)*cos(skew)*cos(slope)
\sin M = &\cos \delta \sin H \cos \xi \cos \eta -\cos \delta \cos H (-\sin \lambda \sin \eta +  \cos \lambda \sin \xi \cos \eta) \nonumber\\
&-\sin \delta(\sin \lambda \sin \xi \cos \eta + \cos \lambda \sin \eta ),
\end{align}
\begin{align}
\label{eq:ewan2}
\cos M  \sin T =& \cos \delta \sin H \sin \eta \cos \xi - \cos \delta \cos H (\sin \lambda \cos \eta + \sin \eta \sin \xi \cos \lambda )\nonumber \\
&+ \sin \delta(\cos \lambda \cos \eta-\sin \lambda \sin \eta \sin \xi )  ,
\end{align}
\begin{align}
\label{eq:ewan3}
\cos M \cos T = \sin \delta \sin \lambda \cos \xi + \sin H \sin \xi \cos \delta + \cos \delta \cos H \cos \lambda \cos \xi.
\end{align}
In this code, the values of the parameters are exactly the ones defined in John's code (that, as already stated above, are not consistently used in the calculations of $T$ and $M$).

Analytically, his first formula agrees with John's if one assumes $\sin \eta\approx \eta$ and $\cos \eta\approx 1$; but this formula doesn't agree either with the one in the Doc or my formula.
Numerically, however, all formulas for the calculation of $M$ seem to be equivalent, but this is simply a coincidence: if one artificially increases the values of the parameters, the difference become noticeable.

Furthermore, Ewan's second formula, Eq. (\ref{eq:ewan2}), is analytically inconsistent with John's second formula, Eq. (\ref{eq:john2}):
even if numerically both formulas are similar, the first term in John's formula is negative, whereas Ewan's first term is positive.

\subsection{Duncan's code}
According to \verb| TCC_Master_Control.c|,
\begin{align}
%MD_Tilt = asin((0.9999940546*cos(Dec_Radians)*sin(Hour_Angle))-(0.0029798011806*cos(Dec_Radians)*cos(Hour_Angle))+(0.002015514993*sin(Dec_Radians)));
\sin M=&\cos \delta \sin H ~ (0.9999940546) -\cos \delta \cos H~(2.9798011806\times 10^{-3})\nonumber \\
&-\sin \delta ~ (-2.015514993\times 10^{-3}).
\end{align}
\begin{align}
%NS_Tilt = acos(((0.003448275861*cos(Dec_Radians)*sin(Hour_Angle))+(0.8154066339*cos(Dec_Radians)*cos(Hour_Angle))-(0.578878472*sin(Dec_Radians)))/cos(MD_Tilt));
\cos M \cos T =&\cos \delta \sin H (3.448275861\times 10^{-3})+\cos \delta \cos H (0.8154066339)\nonumber\\
&-\sin \delta (0.578878472).
\end{align}
This code seems to resemble the formulae in the Doc, but the first formula seems to have a typo, if one compares the coefficient $2.9798011806\times 10^{-3}$ with the one in the Doc, $2.798011806\times 10{-3}$.
If that is indeed a typing mistake, and given that the calculation of $T$ depends on the value of $M$ (that contains the typo), then both $M$ and $T$ are wrong.

\section{Conclusions}
\begin{itemize}
\item My formulas are consistent with the ones in the Doc (if one changes the signs of $\xi$ and $\eta$).
\item John's code is inconsistent: it assumes different values of the parameters for the calculations of $T$ and $M$.
Furthermore, it uses some approximations (which in principle should be accurate enough), but, more importantly, the formula for $T$ is analytically different from the one in the Doc.
The numerical discrepancies are small (because $\eta$ happens to be small).
\item Ewan's formulas are analytically different from all others, although numerically the difference is small (at least for the direct coordinate transformation).
Besides, the derivation of the coordinate transformations uses six rotations and one reflection, which I can't understand.
In principle 3 rotations should be enough to account for the telescope's orientation and location.
\item Duncan's code should not be used: it contains a typo that affects the calculations of both $T$ and $M$.
\end{itemize}

Summarising,
\[
\hspace*{-1cm}\boxed{
\begin{array}{l}
T=\arctan \left( \frac{\sin H \cos \delta \sin \eta-\cos H \cos \delta \sin \lambda \cos \eta + \sin \delta \cos \lambda \cos \eta}{-\sin H \cos \delta \cos \eta \sin \xi + \cos H \cos \delta (\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi) + \sin \delta (\sin \lambda \cos \xi +\cos \lambda \sin \eta \sin \xi)} \right), \vspace{0.5cm}\\

M=\arcsin \left( \sin H \cos \delta \cos \eta \cos \xi+\cos H \cos \delta (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi) +\right.\nonumber \\
 \left. \hspace*{2.3cm} \sin \delta (\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi)\right).
\end{array}}
\]
\[
\hspace*{-1cm}\boxed{
\begin{array}{l}
H=\arctan \left( \frac{\sin M \cos \eta \cos \xi + \sin T \cos M \sin \eta -\cos T \cos M \cos \eta \sin \xi}{-\sin T \cos M \sin \lambda \cos \eta+\cos T \cos M(\cos \lambda \cos \xi-\sin \lambda \sin \eta \sin \xi)+\sin M (\cos \lambda \sin \xi+\sin \lambda \sin \eta \cos \xi)} \right), \vspace{0.5cm}\\

\delta=\arcsin \left( \sin T \cos M \cos \lambda \cos \eta +\cos T \cos M (\sin \lambda \cos \xi+\cos \lambda \sin \eta \sin \xi)+\right. \nonumber \\
 \left. \hspace*{2cm} \sin M(\sin \lambda \sin \xi-\cos \lambda \sin \eta \cos \xi) \right).
\end{array}}
\]
where the parameters are
\begin{itemize}
\item $\lambda=-35.370707^\circ=-0.617335295908206$.
\item $\eta=-4.9''$.
\item $\xi=-1/289.9=-3.4494653328734047\times 10^{-3}$.
\end{itemize}
which are the ones in the Doc, after changing the sign of $\eta$ and $\xi$.
%Given that, in the Doc, $\xi>0$ and $\eta>0$, This implies that $\hat{\eta}>0$ and $\hat{\xi}>0$, and hence my initially defined $\xi$ and $\eta$ must be negative.
This has the following physical implications:

\noindent\fbox{
\parbox{\textwidth}{
$\to$ The easternmost side is tilted towards the South.

$\to$ The easternmost side is higher from the ground.
}}

\rosa{Chris' last email contradicts these conclusions. We should confirm the signs of $\xi$ and $\eta$. If we are sure about the physical orientation of the telescope, then we can forget about the parameters given in the Doc, and simply stick to the last formulas with the correct signs of the angles.}

%\bibliography{coords}

\end{document}

%\begin{figure}[tbhp]
%\centering
%\includegraphics[angle=270,width=\textwidth]{cal2grueso.eps}
%\caption{Calibration of the thin detector. Counts obtained by the thick detector.}
%\end{figure}

%\tiny 55 
%\scriptsize 77 
%\footnotesize 88 
%\small 99 
%\normalsize 1010 
%\large 1212 
%%\Large 1414.40 
%\LARGE 1817.28 
%\huge 2020.74 
%\Huge 2424.88
